﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Excel.Application;
using Border = Microsoft.Office.Interop.Word.Border;
using Range = Microsoft.Office.Interop.Excel.Range;
using _Application = Microsoft.Office.Interop.Excel._Application;

namespace def
{
    public partial class Corporation : ISerializable, IXmlSerializable
    {
        public Corporation(SerializationInfo info, StreamingContext context)
        {
            _employees = (Employees) info.GetValue("employees", typeof(Employees));
            _departments = (Departments) info.GetValue("departments", typeof(Departments));
            _workers = (Workers) info.GetValue("workers", typeof(Workers));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("employees", _employees, typeof(Employees));
            info.AddValue("departments", _departments, typeof(Departments));
            info.AddValue("workers", _workers, typeof(Workers));
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("CountEmployees", _employees.Count.ToString());
            for (var index = 0; index < _employees.Count; index++)
            {
                writer.WriteAttributeString("TableNumber" + index, _employees[index].TableNumber.ToString());
                writer.WriteAttributeString("Name" + index, _employees[index].Name);
                writer.WriteAttributeString("Surname" + index, _employees[index].Surname);
                writer.WriteAttributeString("Patronymic" + index, _employees[index].Patronymic);
                writer.WriteAttributeString("BirthDate" + index,
                    _employees[index].BirthDate.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString("Education" + index, _employees[index].Education.ToString());
                writer.WriteAttributeString("EmploymentDate" + index,
                    _employees[index].EmploymentDate.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString("Seniority" + index, _employees[index].Seniority.ToString());
            }
            writer.WriteAttributeString("CountDepartments", _departments.Count.ToString());
            for (var index = 0; index < _departments.Count; index++)
            {
                writer.WriteAttributeString("DepartmentNumber" + index,
                    _departments[index].DepartmentNumber.ToString());
                writer.WriteAttributeString("DepartmentName" + index, _departments[index].DepartmentName);
                writer.WriteAttributeString("DepartmentType" + index, _departments[index].DepartmentType.ToString());
                writer.WriteAttributeString("EmployeeNumber" + index, _departments[index].TableNumber.ToString());
            }
            writer.WriteAttributeString("CountWorkers", _workers.Count.ToString());
            for (var index = 0; index < _workers.Count; index++)
            {
                writer.WriteAttributeString("DocumentDate" + index,
                    _workers[index].DocumentDate.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString("PKtableNum" + index, _workers[index].TableNumber.ToString());
                writer.WriteAttributeString("PKdepartmentNum" + index, _workers[index].DepartmentNumber.ToString());
                writer.WriteAttributeString("OrderNumber" + index, _workers[index].OrderNumber);
                writer.WriteAttributeString("Salary" + index, _workers[index].Salary.ToString());
            }
        }

        public void ReadXml(XmlReader reader)
        {
            var countEmployees = int.Parse(reader["CountEmployees"] ?? throw new InvalidOperationException());
            for (var i1 = 0; i1 < countEmployees; i1++)
            {
                var employee = new Employee
                {
                    TableNumber = Guid.Parse(reader["TableNumber" + i1] ?? throw new InvalidOperationException()),
                    Name = reader["Name" + i1],
                    Surname = reader["Surname" + i1],
                    Patronymic = reader["Patronymic" + i1],
                    BirthDate = DateTime.Parse(reader["BirthDate" + i1], CultureInfo.InvariantCulture)
                };
                var edutype = reader["Education" + i1];
                switch (edutype)
                {
                    case "Intermediate":
                    {
                        employee.Education = Employee.EducationEnum.Intermediate;
                        break;
                    }
                    case "IntermediateSpecial":
                    {
                        employee.Education = Employee.EducationEnum.IntermediateSpecial;
                        break;
                    }
                    case "Highter":
                    {
                        employee.Education = Employee.EducationEnum.Highter;
                        break;
                    }
                }
                employee.EmploymentDate = DateTime.Parse(reader["EmploymentDate" + i1], CultureInfo.InvariantCulture);
                employee.Seniority = short.Parse(reader["Seniority" + i1] ?? throw new InvalidOperationException());
                _employees.Add(employee);
            }
            var countDepartments = int.Parse(reader["CountDepartments"] ?? throw new InvalidOperationException());
            reader.MoveToNextAttribute();
            for (var i1 = 0; i1 < countDepartments; i1++)
            {
                var department = new Department
                {
                    DepartmentNumber =
                        Guid.Parse(reader["DepartmentNumber" + i1] ?? throw new InvalidOperationException()),
                    DepartmentName = reader["DepartmentName" + i1]
                };
                var deptype = reader["DepartmentType" + i1];
                switch (deptype)
                {
                    case "Workshop":
                    {
                        department.DepartmentType = Department.DepartmentTypeEnum.Workshop;
                        break;
                    }
                    case "Department":
                    {
                        department.DepartmentType = Department.DepartmentTypeEnum.Department;
                        break;
                    }
                    case "Administration":
                    {
                        department.DepartmentType = Department.DepartmentTypeEnum.Administration;
                        break;
                    }
                }
                department.TableNumber =
                    Guid.Parse(reader["TableNumber" + i1] ?? throw new InvalidOperationException());
                _departments.Add(department);
            }
            var countWorkers = int.Parse(reader["CountWorkers"] ?? throw new InvalidOperationException());
            for (var i1 = 0; i1 < countWorkers; i1++)
            {
                var worker = new Worker
                {
                    DocumentDate = DateTime.Parse(reader["DocumentDate" + i1], CultureInfo.InvariantCulture),
                    TableNumber = Guid.Parse(reader["EmployeeNumber" + i1] ?? throw new InvalidOperationException()),
                    DepartmentNumber =
                        Guid.Parse(reader["PKdepartmentNum" + i1] ?? throw new InvalidOperationException()),
                    OrderNumber = reader["OrderNumber" + i1],
                    Salary = short.Parse(reader["Salary" + i1] ?? throw new InvalidOperationException())
                };
                _workers.Add(worker);
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void SaveBinary(string filename)
        {
            var binaryFormatter = new BinaryFormatter();
            using (var fileStream = new FileStream(filename, FileMode.Create))
            {
                binaryFormatter.Serialize(fileStream, this);
                fileStream.Close();
            }
        }

        public static Corporation OpenBinary(string filename)
        {
            Corporation corporation;
            var binaryFormatter = new BinaryFormatter();
            using (var fileStream = new FileStream(filename, FileMode.Open))
            {
                corporation = (Corporation) binaryFormatter.Deserialize(fileStream);
                fileStream.Close();
            }
            return corporation;
        }

        public void SaveXml(string filename)
        {
            var xmlSerializer = new XmlSerializer(typeof(Corporation));
            using (var fileStream = new FileStream(filename, FileMode.Create))
            {
                xmlSerializer.Serialize(fileStream, this);
                fileStream.Close();
            }
        }

        public static Corporation OpenXml(string filename)
        {
            Corporation corporation;
            var xmlSerializer = new XmlSerializer(typeof(Corporation));
            using (var fileStream = new FileStream(filename, FileMode.Open))
            {
                corporation = (Corporation) xmlSerializer.Deserialize(fileStream);
                fileStream.Close();
            }
            return corporation;
        }

        public void SaveJson(string filename)
        {
            var serializer = new DataContractJsonSerializer(typeof(Corporation));
            using (var fileStream = new FileStream(filename, FileMode.Create))
            {
                serializer.WriteObject(fileStream, this);
                fileStream.Close();
            }
        }

        public static Corporation OpenJson(string filename)
        {
            Corporation corporation;
            var serializer = new DataContractJsonSerializer(typeof(Corporation));
            using (var fileStream = new FileStream(filename, FileMode.Open))
            {
                corporation = (Corporation) serializer.ReadObject(fileStream);
                fileStream.Close();
            }
            return corporation;
        }

        public void SaveToExcel(string filename)
        {
            object missing = Missing.Value;
            _Application xlApp = new Application();
            var xlBook = xlApp.Workbooks.Add();
            var xlSheet = (Worksheet) xlBook.Worksheets.Item[1];
            var rowIndex = 1;
            xlSheet.Cells[rowIndex, 1] = "Document date";
            xlSheet.Cells[rowIndex, 2] = "Table number";
            xlSheet.Cells[rowIndex, 3] = "Department number";
            xlSheet.Cells[rowIndex, 4] = "Order number";
            xlSheet.Cells[rowIndex++, 5] = "Salary";
            ((Range) xlSheet.Columns[1, Type.Missing]).ColumnWidth = 15;
            ((Range) xlSheet.Columns[2, Type.Missing]).ColumnWidth = 30;
            ((Range) xlSheet.Columns[3, Type.Missing]).ColumnWidth = 30;
            ((Range) xlSheet.Columns[4, Type.Missing]).ColumnWidth = 12;
            ((Range) xlSheet.Columns[5, Type.Missing]).ColumnWidth = 12;
            foreach (var worker in Workers)
            {
                xlSheet.Cells[rowIndex, 1] = worker.DocumentDate.ToString(CultureInfo.InvariantCulture);
                xlSheet.Cells[rowIndex, 2] = worker.TableNumber.ToString();
                xlSheet.Cells[rowIndex, 3] = worker.DepartmentNumber.ToString();
                xlSheet.Cells[rowIndex, 4] = worker.OrderNumber;
                xlSheet.Cells[rowIndex++, 5] = worker.Salary.ToString();
            }
            xlBook.Sheets.Add();
            xlSheet = (Worksheet) xlBook.Worksheets.Item["Лист2"];
            rowIndex = 1;
            xlSheet.Cells[rowIndex, 1] = "Department number";
            xlSheet.Cells[rowIndex, 2] = "Department name";
            xlSheet.Cells[rowIndex, 3] = "Department type";
            xlSheet.Cells[rowIndex++, 4] = "Boss number";
            ((Range) xlSheet.Columns[1, Type.Missing]).ColumnWidth = 30;
            ((Range) xlSheet.Columns[2, Type.Missing]).ColumnWidth = 12;
            ((Range) xlSheet.Columns[3, Type.Missing]).ColumnWidth = 12;
            ((Range) xlSheet.Columns[4, Type.Missing]).ColumnWidth = 30;
            foreach (var department in Departments)
            {
                xlSheet.Cells[rowIndex, 1] = department.DepartmentNumber.ToString();
                xlSheet.Cells[rowIndex, 2] = department.DepartmentName;
                xlSheet.Cells[rowIndex, 3] = department.DepartmentType;
                xlSheet.Cells[rowIndex++, 4] = department.TableNumber.ToString();
            }
            xlBook.Sheets.Add();
            xlSheet = (Worksheet) xlBook.Worksheets.Item["Лист3"];
            rowIndex = 1;
            xlSheet.Cells[rowIndex, 1] = "Table number";
            xlSheet.Cells[rowIndex, 2] = "Name";
            xlSheet.Cells[rowIndex, 3] = "Surname";
            xlSheet.Cells[rowIndex, 4] = "Patronymic";
            xlSheet.Cells[rowIndex, 5] = "Birthdate";
            xlSheet.Cells[rowIndex, 6] = "Education";
            xlSheet.Cells[rowIndex, 7] = "Employment date";
            xlSheet.Cells[rowIndex++, 8] = "Experience";
            ((Range) xlSheet.Columns[1, Type.Missing]).ColumnWidth = 30;
            ((Range) xlSheet.Columns[2, Type.Missing]).ColumnWidth = 12;
            ((Range) xlSheet.Columns[3, Type.Missing]).ColumnWidth = 12;
            ((Range) xlSheet.Columns[4, Type.Missing]).ColumnWidth = 12;
            ((Range) xlSheet.Columns[5, Type.Missing]).ColumnWidth = 15;
            ((Range) xlSheet.Columns[6, Type.Missing]).ColumnWidth = 10;
            ((Range) xlSheet.Columns[7, Type.Missing]).ColumnWidth = 15;
            ((Range) xlSheet.Columns[8, Type.Missing]).ColumnWidth = 10;
            foreach (var employee in Employees)
            {
                xlSheet.Cells[rowIndex, 1] = employee.TableNumber.ToString();
                xlSheet.Cells[rowIndex, 2] = employee.Name;
                xlSheet.Cells[rowIndex, 3] = employee.Surname;
                xlSheet.Cells[rowIndex, 4] = employee.Patronymic;
                xlSheet.Cells[rowIndex, 5] = employee.BirthDate.ToString(CultureInfo.InvariantCulture);
                xlSheet.Cells[rowIndex, 6] = employee.Education;
                xlSheet.Cells[rowIndex, 7] = employee.EmploymentDate.ToString(CultureInfo.InvariantCulture);
                xlSheet.Cells[rowIndex++, 8] = employee.Seniority.ToString();
            }
            xlBook.SaveAs(filename);
            xlBook.Close(true, missing, missing);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlSheet);
            Marshal.ReleaseComObject(xlBook);
            Marshal.ReleaseComObject(xlApp);
        }

        public static Corporation OpenExcel(string filename)
        {
            var corporation = new Corporation();
            object missing = Missing.Value;
            _Application xlApp = new Application();
            var xlBook = xlApp.Workbooks.Open(filename, 0, false, 5, "", "", true,
                XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            var xlSheet = (Worksheet) xlBook.Worksheets.Item[1];
            var usedRowsCount = xlSheet.UsedRange.Rows.Count;
            int columnIndex;
            for (var rowIndex = 2; rowIndex <= usedRowsCount; rowIndex++)
            {
                columnIndex = 1;
                var worker = new Worker
                {
                    DocumentDate =
                        DateTime.FromOADate(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2),
                    TableNumber = Guid.Parse(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2),
                    DepartmentNumber = Guid.Parse(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2),
                    OrderNumber = ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2,
                    Salary = (short) ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex]).Value2
                };
                corporation.Workers.Add(worker);
            }
            xlSheet = (Worksheet) xlBook.Worksheets.Item["Лист2"];
            usedRowsCount = xlSheet.UsedRange.Rows.Count;
            for (var rowIndex = 2; rowIndex <= usedRowsCount; rowIndex++)
            {
                columnIndex = 1;
                var department = new Department
                {
                    DepartmentNumber = Guid.Parse(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2),
                    DepartmentName = ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2
                };
                var deptype = (short) ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2;
                switch (deptype)
                {
                    case 0:
                    {
                        department.DepartmentType = Department.DepartmentTypeEnum.Workshop;
                        break;
                    }
                    case 1:
                    {
                        department.DepartmentType = Department.DepartmentTypeEnum.Department;
                        break;
                    }
                    case 2:
                    {
                        department.DepartmentType = Department.DepartmentTypeEnum.Administration;
                        break;
                    }
                }
                department.TableNumber = Guid.Parse(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex]).Value2);
                corporation.Departments.Add(department);
            }
            xlSheet = (Worksheet) xlBook.Worksheets.Item["Лист3"];
            usedRowsCount = xlSheet.UsedRange.Rows.Count;
            for (var rowIndex = 2; rowIndex <= usedRowsCount; rowIndex++)
            {
                columnIndex = 1;
                var employee = new Employee
                {
                    TableNumber = Guid.Parse(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2),
                    Name = ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2,
                    Surname = ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2,
                    Patronymic = ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2,
                    BirthDate = DateTime.FromOADate(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2)
                };
                var edutype = (short) ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2;
                switch (edutype)
                {
                    case 0:
                    {
                        employee.Education = Employee.EducationEnum.Intermediate;
                        break;
                    }
                    case 1:
                    {
                        employee.Education = Employee.EducationEnum.IntermediateSpecial;
                        break;
                    }
                    case 2:
                    {
                        employee.Education = Employee.EducationEnum.Highter;
                        break;
                    }
                }
                employee.EmploymentDate =
                    DateTime.FromOADate(((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex++]).Value2);
                employee.Seniority = (short) ((Range) xlSheet.UsedRange.Cells[rowIndex, columnIndex]).Value2;
                corporation.Employees.Add(employee);
            }
            xlBook.Close(true, missing, missing);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlSheet);
            Marshal.ReleaseComObject(xlBook);
            Marshal.ReleaseComObject(xlApp);
            return corporation;
        }

        public void SaveWord(string filename)
        {
            Microsoft.Office.Interop.Word._Application word = new Microsoft.Office.Interop.Word.Application();
            var document = word.Documents.Add();
            var fitBehavior = Type.Missing;
            document.Paragraphs.Add();
            document.Paragraphs[1].Range.Text = "Employees\r\n";
            var rowsCount = Employees.Count;
            document.Tables.Add(document.Paragraphs[document.Paragraphs.Count].Range, rowsCount + 1, 8, ref fitBehavior,
                ref fitBehavior);
            var borders = new Border[6];
            var table = document.Tables[1];
            borders[0] = table.Borders[WdBorderType.wdBorderLeft];
            borders[1] = table.Borders[WdBorderType.wdBorderRight];
            borders[2] = table.Borders[WdBorderType.wdBorderTop];
            borders[3] = table.Borders[WdBorderType.wdBorderBottom];
            borders[4] = table.Borders[WdBorderType.wdBorderHorizontal];
            borders[5] = table.Borders[WdBorderType.wdBorderVertical];
            foreach (var border in borders)
            {
                border.LineStyle = WdLineStyle.wdLineStyleSingle;
                border.Color = WdColor.wdColorBlack;
            }
            document.Tables[1].Cell(1, 1).Range.Text = "Table number";
            document.Tables[1].Cell(1, 2).Range.Text = "Surname";
            document.Tables[1].Cell(1, 3).Range.Text = "Name";
            document.Tables[1].Cell(1, 4).Range.Text = "Patronymic";
            document.Tables[1].Cell(1, 5).Range.Text = "Birthdate";
            document.Tables[1].Cell(1, 6).Range.Text = "Education";
            document.Tables[1].Cell(1, 7).Range.Text = "Appliance date";
            document.Tables[1].Cell(1, 8).Range.Text = "Experience";
            var row = 2;
            foreach (var employee in Employees)
            {
                document.Tables[1].Cell(row, 1).Range.Text = employee.TableNumber.ToString();
                document.Tables[1].Cell(row, 2).Range.Text = employee.Surname;
                document.Tables[1].Cell(row, 3).Range.Text = employee.Name;
                document.Tables[1].Cell(row, 4).Range.Text = employee.Patronymic;
                document.Tables[1].Cell(row, 5).Range.Text = employee.BirthDate.ToString(CultureInfo.InvariantCulture);
                document.Tables[1].Cell(row, 6).Range.Text = employee.Education.ToString();
                document.Tables[1].Cell(row, 7).Range.Text =
                    employee.EmploymentDate.ToString(CultureInfo.InvariantCulture);
                document.Tables[1].Cell(row++, 8).Range.Text = employee.Seniority.ToString();
            }
            document.Paragraphs.Add();
            document.Paragraphs[document.Paragraphs.Count].Range.Text = "Departments\r\n";
            rowsCount = Departments.Count;
            document.Tables.Add(document.Paragraphs[document.Paragraphs.Count].Range, rowsCount + 1, 4, ref fitBehavior,
                ref fitBehavior);
            borders = new Border[6];
            table = document.Tables[document.Tables.Count];
            borders[0] = table.Borders[WdBorderType.wdBorderLeft];
            borders[1] = table.Borders[WdBorderType.wdBorderRight];
            borders[2] = table.Borders[WdBorderType.wdBorderTop];
            borders[3] = table.Borders[WdBorderType.wdBorderBottom];
            borders[4] = table.Borders[WdBorderType.wdBorderHorizontal];
            borders[5] = table.Borders[WdBorderType.wdBorderVertical];
            foreach (var border in borders)
            {
                border.LineStyle = WdLineStyle.wdLineStyleSingle;
                border.Color = WdColor.wdColorBlack;
            }
            document.Tables[document.Tables.Count].Cell(1, 1).Range.Text = "Department number";
            document.Tables[document.Tables.Count].Cell(1, 2).Range.Text = "Department name";
            document.Tables[document.Tables.Count].Cell(1, 3).Range.Text = "Department type";
            document.Tables[document.Tables.Count].Cell(1, 4).Range.Text = "Boss number";
            row = 2;
            foreach (var department in Departments)
            {
                document.Tables[document.Tables.Count].Cell(row, 1).Range.Text = department.DepartmentNumber.ToString();
                document.Tables[document.Tables.Count].Cell(row, 2).Range.Text = department.DepartmentName;
                document.Tables[document.Tables.Count].Cell(row, 3).Range.Text = department.DepartmentType.ToString();
                document.Tables[document.Tables.Count].Cell(row++, 4).Range.Text = department.TableNumber.ToString();
            }
            document.Paragraphs.Add();
            document.Paragraphs[document.Paragraphs.Count].Range.Text = "Workers\r\n";
            rowsCount = Workers.Count;
            document.Tables.Add(document.Paragraphs[document.Paragraphs.Count].Range, rowsCount + 1, 5, ref fitBehavior,
                ref fitBehavior);
            borders = new Border[6];
            table = document.Tables[document.Tables.Count];
            borders[0] = table.Borders[WdBorderType.wdBorderLeft];
            borders[1] = table.Borders[WdBorderType.wdBorderRight];
            borders[2] = table.Borders[WdBorderType.wdBorderTop];
            borders[3] = table.Borders[WdBorderType.wdBorderBottom];
            borders[4] = table.Borders[WdBorderType.wdBorderHorizontal];
            borders[5] = table.Borders[WdBorderType.wdBorderVertical];
            foreach (var border in borders)
            {
                border.LineStyle = WdLineStyle.wdLineStyleSingle;
                border.Color = WdColor.wdColorBlack;
            }
            document.Tables[document.Tables.Count].Cell(1, 1).Range.Text = "Appliance date";
            document.Tables[document.Tables.Count].Cell(1, 2).Range.Text = "Table number";
            document.Tables[document.Tables.Count].Cell(1, 3).Range.Text = "Department number";
            document.Tables[document.Tables.Count].Cell(1, 4).Range.Text = "Order number";
            document.Tables[document.Tables.Count].Cell(1, 5).Range.Text = "Salary";
            row = 2;
            foreach (var worker in Workers)
            {
                document.Tables[document.Tables.Count].Cell(row, 1).Range.Text =
                    worker.DocumentDate.ToString(CultureInfo.InvariantCulture);
                document.Tables[document.Tables.Count].Cell(row, 2).Range.Text = worker.TableNumber.ToString();
                document.Tables[document.Tables.Count].Cell(row, 3).Range.Text = worker.DepartmentNumber.ToString();
                document.Tables[document.Tables.Count].Cell(row, 4).Range.Text = worker.OrderNumber;
                document.Tables[document.Tables.Count].Cell(row++, 5).Range.Text = worker.Salary.ToString();
            }
            document.SaveAs(filename);
            document.Close();
            word.Quit();
            Marshal.ReleaseComObject(document);
            Marshal.ReleaseComObject(word);
        }
    }
}