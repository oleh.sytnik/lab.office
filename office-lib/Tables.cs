﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Xml.Serialization;

namespace def
{
    [Serializable]
    public class Employee
    {
        [XmlAttribute]
        public Guid TableNumber { get; set; }

        [XmlAttribute]
        public string Surname { get; set; }

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Patronymic { get; set; }

        [XmlAttribute]
        public DateTime BirthDate { get; set; }

        [XmlAttribute]
        public EducationEnum Education { get; set; }

        [XmlAttribute]
        public DateTime EmploymentDate { get; set; }

        [XmlAttribute]
        public short Seniority { get; set; }

        public enum EducationEnum
        {
            Intermediate,
            IntermediateSpecial,
            Highter
        };

        public Employee()
        {
            TableNumber = Guid.NewGuid();
            Surname = "Surname";
            Name = "Name";
            Patronymic = "Patronymic";
            BirthDate = DateTime.Now;
            Education = EducationEnum.Intermediate;
            EmploymentDate = DateTime.Now;
            Seniority = 0;
        }
    }

    [Serializable]
    public class Department
    {
        [XmlAttribute]
        public Guid DepartmentNumber { get; set; }

        [XmlAttribute]
        public string DepartmentName { get; set; }

        [XmlAttribute]
        public DepartmentTypeEnum DepartmentType { get; set; }

        [XmlAttribute]
        public Guid TableNumber { get; set; }

        public enum DepartmentTypeEnum
        {
            Workshop,
            Department,
            Administration
        };

        public Department()
        {
            DepartmentNumber = Guid.NewGuid();
            DepartmentName = "Department";
            DepartmentType = DepartmentTypeEnum.Workshop;
            TableNumber = Guid.Empty;
        }
    }

    [Serializable]
    public class Worker
    {
        [XmlAttribute]
        public DateTime DocumentDate { get; set; }

        [XmlAttribute]
        public Guid TableNumber { get; set; }

        [XmlAttribute]
        public Guid DepartmentNumber { get; set; }

        [XmlAttribute]
        public string OrderNumber { get; set; }

        [XmlAttribute]
        public int Salary { get; set; }
    }

    [Serializable]
    public class Employees : ObservableCollection<Employee>
    {
    }

    [Serializable]
    public class Departments : ObservableCollection<Department>
    {
    }

    [Serializable]
    public class Workers : List<Worker>
    {
    }

    [Serializable]
    public partial class Corporation
    {
        [XmlElement] private Departments _departments;
        [XmlElement] private Employees _employees;
        [XmlElement] private Workers _workers;

        public Employees Employees
        {
            get => _employees;
            set => _employees = value;
        }

        public Departments Departments
        {
            get => _departments;
            set => _departments = value;
        }

        public Workers Workers
        {
            get => _workers;
            set => _workers = value;
        }

        public Corporation()
        {
            Employees = new Employees();
            Departments = new Departments();
            Workers = new Workers();
            Employees.CollectionChanged += EmployeesCollectionChanged;
            Departments.CollectionChanged += DepartmentsCollectionChanged;
        }

        private void EmployeesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Remove) return;
            var oldItem = (Employee) e.OldItems[0];
            Workers.RemoveAll(worker => worker.TableNumber.ToString() == oldItem.TableNumber.ToString());
        }

        private void DepartmentsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Remove) return;
            var oldItem = (Department) e.OldItems[0];
            Workers.RemoveAll(worker => worker.DepartmentNumber == oldItem.TableNumber);
        }

        public bool EnableAddWorker => Departments.Count > 0 && Employees.Count > 0;
    }
}