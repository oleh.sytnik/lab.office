﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace def
{
    internal partial class Base : Form
    {
        private Corporation _corporation;

        internal Base()
        {
            InitializeComponent();
        }

        private void FormLoadHandler(object sender, EventArgs e)
        {
            _corporation = new Corporation();
            colEducation.DataSource = Enum.GetValues(typeof(Employee.EducationEnum));
            colEducation.ValueType = typeof(Employee.EducationEnum);
            SetDefaultData();
            SFD.Filter =
                @"Binary (.bin)|*.bin|XML (.xml)|*.xml|Json (.json)|*.json|Excel (.xlsx)|*.xlsx|Word (.docx)|*.docx";
            OFD.Filter = @"Binary (.bin)|*.bin|XML (.xml)|*.xml|Json (.json)|*.json|Exel (.xlsx)|*.xlsx";
            DepartmentsBS.DataSource = _corporation.Departments;
            EmployeesBS.DataSource = _corporation.Employees;
            WorkersBS.DataSource = _corporation.Workers;
        }

        private void OpenFileHandler(object sender, EventArgs e)
        {
            var dialogResult = OFD.ShowDialog();
            if (dialogResult != DialogResult.OK) return;
            switch (OFD.FilterIndex)
            {
                case 1:
                    _corporation = Corporation.OpenBinary(OFD.FileName);
                    break;
                case 2:
                    _corporation = Corporation.OpenXml(OFD.FileName);
                    break;
                case 3:
                    _corporation = Corporation.OpenJson(OFD.FileName);
                    break;
                case 4:
                    _corporation = Corporation.OpenExcel(OFD.FileName);
                    break;
            }
            DepartmentsBS.DataSource = _corporation.Departments;
            EmployeesBS.DataSource = _corporation.Employees;
            WorkersBS.DataSource = _corporation.Workers;
        }

        private void SaveFileHandler(object sender, EventArgs e)
        {
            var dialogResult = SFD.ShowDialog();
            if (dialogResult != DialogResult.OK) return;
            switch (SFD.FilterIndex)
            {
                case 1:
                    _corporation.SaveBinary(SFD.FileName);
                    break;
                case 2:
                    _corporation.SaveXml(SFD.FileName);
                    break;
                case 3:
                    _corporation.SaveJson(SFD.FileName);
                    break;
                case 4:
                    _corporation.SaveToExcel(SFD.FileName);
                    break;
                case 5:
                    _corporation.SaveWord(SFD.FileName);
                    break;
            }
        }

        private void NewWorkerHandler(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new Worker
            {
                DocumentDate = DateTime.Now,
                TableNumber = _corporation.Employees[0].TableNumber,
                DepartmentNumber = _corporation.Departments[0].DepartmentNumber,
                OrderNumber = "___-__/__",
                Salary = 1000
            };
        }

        private void WorkersDataChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
                WorkersBS.ResetBindings(false);
            WorkersBS.AllowNew = _corporation != null && _corporation.EnableAddWorker;
        }

        private void WorkersDataErrorHandler(object sender, DataGridViewDataErrorEventArgs e)
        {
            workersDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Dispose();
        }

        private void SetDefaultData()
        {
            _corporation.Employees.Add(new Employee
            {
                Surname = "Potapov",
                Name = "Nikolai",
                Patronymic = "Alexandrovych",
                Education = Employee.EducationEnum.Highter,
                BirthDate = new DateTime(1987, 2, 25),
                Seniority = 4,
                EmploymentDate = new DateTime(2010, 3, 1)
            });
            _corporation.Employees.Add(new Employee
            {
                Surname = "Barannik",
                Name = "Sergey",
                Patronymic = "Jurievich",
                Education = Employee.EducationEnum.IntermediateSpecial,
                BirthDate = new DateTime(1992, 3, 17),
                Seniority = 5,
                EmploymentDate = new DateTime(2009, 5, 21)
            });

            _corporation.Employees.Add(new Employee
            {
                Surname = "Stepanenko",
                Name = "Vladimir",
                Patronymic = "Igorevich",
                Education = Employee.EducationEnum.Intermediate,
                BirthDate = new DateTime(1980, 7, 13),
                Seniority = 7,
                EmploymentDate = new DateTime(2010, 7, 14)
            });
            _corporation.Employees.Add(new Employee
            {
                Surname = "Sergeeva",
                Name = "Ludmila",
                Patronymic = "Nikolaevna",
                Education = Employee.EducationEnum.Highter,
                BirthDate = new DateTime(1984, 5, 12),
                Seniority = 3,
                EmploymentDate = new DateTime(2011, 6, 17)
            });
            _corporation.Employees.Add(new Employee
            {
                Surname = "Kuptsov",
                Name = "Anton",
                Patronymic = "Vladimirovich",
                Education = Employee.EducationEnum.IntermediateSpecial,
                BirthDate = new DateTime(1985, 1, 16),
                Seniority = 8,
                EmploymentDate = new DateTime(2004, 7, 13)
            });
            _corporation.Departments.Add(new Department
            {
                DepartmentName = "Admin-A1",
                DepartmentType = Department.DepartmentTypeEnum.Administration,
                TableNumber = _corporation.Employees[0].TableNumber
            });
            _corporation.Departments.Add(new Department
            {
                DepartmentName = "Kaf603",
                DepartmentType = Department.DepartmentTypeEnum.Department,
                TableNumber = _corporation.Employees[1].TableNumber
            });
            _corporation.Departments.Add(new Department
            {
                DepartmentName = "Storage-15",
                DepartmentType = Department.DepartmentTypeEnum.Workshop,
                TableNumber = _corporation.Employees[2].TableNumber
            });
            _corporation.Departments.Add(new Department
            {
                DepartmentName = "Part-704",
                DepartmentType = Department.DepartmentTypeEnum.Department,
                TableNumber = _corporation.Employees[3].TableNumber
            });
            _corporation.Departments.Add(new Department
            {
                DepartmentName = "Laboratory-1503",
                DepartmentType = Department.DepartmentTypeEnum.Workshop,
                TableNumber = _corporation.Employees[4].TableNumber
            });
            _corporation.Workers.Add(new Worker
            {
                DocumentDate = new DateTime(2012, 6, 26),
                TableNumber = _corporation.Employees[0].TableNumber,
                DepartmentNumber = _corporation.Departments[0].DepartmentNumber,
                OrderNumber = "122-3f/1w",
                Salary = 2500
            });
            _corporation.Workers.Add(new Worker
            {
                DocumentDate = new DateTime(2012, 3, 28),
                TableNumber = _corporation.Employees[1].TableNumber,
                DepartmentNumber = _corporation.Departments[1].DepartmentNumber,
                OrderNumber = "103-2e/4c",
                Salary = 2250
            });
            _corporation.Workers.Add(new Worker
            {
                DocumentDate = new DateTime(2012, 9, 10),
                TableNumber = _corporation.Employees[2].TableNumber,
                DepartmentNumber = _corporation.Departments[2].DepartmentNumber,
                OrderNumber = "189-0a/8n",
                Salary = 1700
            });
            _corporation.Workers.Add(new Worker
            {
                DocumentDate = new DateTime(2012, 4, 13),
                TableNumber = _corporation.Employees[3].TableNumber,
                DepartmentNumber = _corporation.Departments[3].DepartmentNumber,
                OrderNumber = "167-7b/6g",
                Salary = 5500
            });
            _corporation.Workers.Add(new Worker
            {
                DocumentDate = new DateTime(2012, 1, 7),
                TableNumber = _corporation.Employees[4].TableNumber,
                DepartmentNumber = _corporation.Departments[4].DepartmentNumber,
                OrderNumber = "134-8x/6r",
                Salary = 3300
            });
        }
    }
}